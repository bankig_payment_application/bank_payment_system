package com.collection;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
class Employee1
{
	 int id; String name;

	public Employee1(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + "]";
	}
	 
}
public class MapDemo {
	public static void main(String[] args) {
		Map<Integer,Employee1> namesOfEmployees = new TreeMap<>();
		namesOfEmployees.put(101, new Employee1(101,"Preety"));
		
		System.out.println(namesOfEmployees);
		
	}
	

}
