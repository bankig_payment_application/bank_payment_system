package com.exception;

class UnderAgeException extends Exception
{
    public UnderAgeException (String msg)
    {
        System.out.println ("You are under age!");
    }
}
public class Main
{
    public static void main (String[]args)
    {
        try
        {

            int age = 16;
           
               if (age< 18)
               {
                   throw new UnderAgeException ("You cant Vote"); //throwing user defined exception
               }
            
        }
        catch (UnderAgeException e)
        {
            System.out.println (e);
        }
    }
}